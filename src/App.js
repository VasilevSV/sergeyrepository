import React from 'react';
import './styles/reset.css';
import './styles/fonts.css';
import './styles/style.css';
import './styles/media-queries.css';
import {Header} from './components/header';
import {Main} from './components/main';
import {Footer} from './components/footer'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bagCounter: [],
            heartCounter: []
        };
        this.changeBagCounter = this.changeBagCounter.bind(this);
        this.changeHeartCounter = this.changeHeartCounter.bind(this);
    }


    changeBagCounter(itemName) {
        if (this.state.bagCounter.includes(itemName)) {
            this.setState({
                bagCounter: this.state.bagCounter.filter(i => i !== itemName)
            });

        } else {
            let {bagCounter} = this.state;
            bagCounter.push(itemName);
            this.setState({
                bagCounter: bagCounter
            })
        }
    }

    changeHeartCounter(itemName) {
        if (this.state.heartCounter.includes(itemName)) {
            this.setState({
                heartCounter: this.state.heartCounter.filter(i => i !== itemName)
            });

        } else {
            let {heartCounter} = this.state;
            heartCounter.push(itemName);
            this.setState({
                heartCounter: heartCounter
            })
        }
    }

    render() {
        return (
            <div className="App">
                <Header bagCounter={this.state.bagCounter.length} heartCounter={this.state.heartCounter.length} />
                <Main changeBagCounter={this.changeBagCounter} changeHeartCounter={this.changeHeartCounter}  />
                <Footer />
            </div>
        );
    }
}

export default App;
