export const NewProducts = [
    {
        image: "./img/grid-1.jpg",
        title: "Kristina Oak Table With White Marble Top",
        price: 799.55
    },
    {
        image: "./img/grid-2.jpg",
        title: "Hay - About A Lounge  Chair AAL 94",
        price:  659.55
    },
    {
        image: "./img/grid-3.jpg",
        title: "Activate Facial Mask and Charcoal Soap R",
        price:  129.55
    },
    {
        image: "./img/grid-4.jpg",
        title: "Cocktail Table Walnut XL | YES",
        price:  299.99
    }
]