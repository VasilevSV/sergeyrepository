import React from 'react';
export const Footer = () => (
    <footer>
        <div className="brown">
            <div className="footer1 container">
                <div className="footer1-content">
                    <div className="footer1-content-img">
                        <img src="./img/delivery-icon.png" alt="" />
                    </div>
                    <div className="footer1-content-text">
                        <h3>Free Delivery</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                    </div>
                </div>
                <div className="footer1-content">
                    <div className="footer1-content-img">
                        <img src="./img/easyreturn-icon.png" alt="" />
                    </div>
                    <div className="footer1-content-text">
                        <h3>easy returns</h3>
                        <p>Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis </p>
                    </div>
                </div>
                <div className="footer1-content">
                    <div className="footer1-content-img">
                        <img src="./img/widechoice-icon.png" alt="" />
                    </div>
                    <div className="footer1-content-text">
                        <h3>wide choice</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                    </div>
                </div>
            </div>
        </div>
        <div className="white">
            <div className="footer2 container ">
                <div className="footer2-date">
                    <p>© 2014 Arvi Theme.  Made with love HezyTheme</p>
                </div>
                <div className="footer2-categories">
                    <h3>Categoriest</h3>
                    <ul>
                        <a href="#Accessories"><li>Accessories</li></a>
                        <a href="#Alchohol"><li>Alchohol</li></a>
                        <a href="#Art"><li>Art</li></a>
                        <a href="#Books"><li>Books</li></a>
                        <a href="#Drink"><li>Drink</li></a>
                        <a href="#Electronics"><li>Electronics</li></a>
                        <a href="#Flowers"><li>Flowers & Plant</li></a>
                        <a href="#Food"><li>Food</li></a>
                        <a href="#Gadgets"><li>Gadgets</li></a>
                        <a href="#Garden"><li>Garden</li></a>
                        <a href="#Grocery"><li>Grocery</li></a>
                        <a href="#Home"><li>Home</li></a>
                        <a href="#Jewelry"><li>Jewelry</li></a>
                        <a href="#Kids"><li>Kids & Baby</li></a>
                        <a href="#MFashion"><li>Men's Fashion</li></a>
                        <a href="#Mobile"><li>Mobile</li></a>
                        <a href="#Motorcyles"><li>Motorcycles</li></a>
                        <a href="#Movies"><li>Movies</li></a>
                        <a href="#Music"><li>Music</li></a>
                        <a href="#Office"><li>Office</li></a>
                        <a href="#Pets"><li>Pets</li></a>
                        <a href="#Romantic"><li>Romantic</li></a>
                        <a href="#Sport"><li>Sport</li></a>
                        <a href="#Toys"><li>Toys</li></a>
                        <a href="#Travel"><li>Travel</li></a>
                        <a href="#WFashion"><li>Women's Fashion</li></a>
                    </ul>
                </div>
                <div className="footer2-about">
                    <h3>About</h3>
                     <ul>
                        <a href="#About"><li>About Us</li></a>
                        <a href="#Delivery"><li>Delivery</li></a>
                        <a href="#Testimonials"><li>Testimonials</li></a>
                        <a href="#Contact"><li>Contact</li></a>
                    </ul>
                </div>
                <div className="footer2-socials">
                    <a href=""><img src="./img/socials-f.jpg" alt="" /></a>
                    <a href=""><img src="./img/socials-g.png" alt="" /></a>
                    <a href=""><img src="./img/socials-t.png" alt="" /></a>
                    <a href=""><img src="./img/socials-p.png" alt="" /></a>
                </div>
            </div>
        </div>
    </footer>
);