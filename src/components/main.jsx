import React from 'react';
import {Menu} from './menu';
import {GridItem} from './grid-item';
import {Products} from '../items';
import {NewProducts} from '../newitems';
import {Button} from './button';
import {Slider} from './slider';

export class Main extends React.Component {
    state = {
        products: Products
    };

    addProducts = () => {
        this.setState({products: this.state.products.concat(NewProducts)})
    };

    render() {
        const {products} = this.state;

        return (
            <main>
                <div className="container">
                    <div className="main1">

                        <Menu />

                        <Slider />

                    </div>

                    <div className="grid">

                        {products.map((item, index) =>
                            <GridItem
                                item={item}
                                lable={item.lable}
                                key={item.title + index}
                                changeBagCounter={this.props.changeBagCounter}
                                changeHeartCounter={this.props.changeHeartCounter}
                            />)}

                    </div>

                    <Button onClick={this.addProducts} />

                </div>
            </main>
        );
    };
}