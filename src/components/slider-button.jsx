import React from "react";

export class SliderButton extends React.Component {
  showPartSlide = () => {
    this.props.onClick(this.props.index);
  };

  render() {
    return (
      <button className="choosed-slide" onClick={this.showPartSlide} style={this.props.style} >
        o
      </button>
    );
  }
}
