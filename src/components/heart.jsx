import React from 'react';

export const Heart = ({counter}) => (
    <a href="">
        <span>{counter}</span>
        <img src="./img/heart-icon.png" alt="heart" />
    </a>
);