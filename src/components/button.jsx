import React from 'react';

export const Button = ({onClick}) => (
    <div className="main-button">
        <button className="view-more" onClick={onClick}>view more</button>
    </div>
);