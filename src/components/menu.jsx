import React from 'react';
export const Menu = () => (
    <div className="categories-menu">
        <ul>
            <a href="#Accessories"><li>Accessories</li></a>
            <a href="#Alchohol"><li>Alchohol</li></a>
            <a href="#Art"><li>Art</li></a>
            <a href="#Books"><li>Books</li></a>
            <a href="#Electronics"><li>Electronics</li></a>
            <a href="#Flower"><li>Flower & Plants</li></a>
            <a href="#Food"><li>Food</li></a>
            <a href="#Gadgets"><li>Gadgets</li></a>
            <a href="#Garden"><li>Garden</li></a>
            <a href="#Grocery"><li>Grocery</li></a>
            <a href="#Home"><li>Home</li></a>
            <a href="#Jewelry"><li>Jewelry</li></a>
            <a href="#Kids"><li>Kids & Baby</li></a>
            <a href="#MFashion"><li>Men's Fashion</li></a>
        </ul>
    </div>
);