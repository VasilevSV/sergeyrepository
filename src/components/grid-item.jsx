import React from 'react';

export class GridItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {favorites: false, wanted: false};
    }

    addToFavorites = () => {
        this.setState({favorites: !this.state.favorites});
        this.props.changeHeartCounter(this.props.item.title);
    }
    addToWanted = () => {
        this.setState({wanted: !this.state.wanted});
        this.props.changeBagCounter(this.props.item.title);
    }
    
    render() {
        const {lable, item} = this.props;
        const {favorites, wanted} = this.state;

        return (
            lable ?
                <div className={`grid-item lable-${lable}`}>
                    <div className="grid-lable">
                        <div className="grid-lable-category">
                            <h3>Categories</h3>
                        </div>
                        <div className="grid-lable-img">
                            <img src={item.image} alt={item.title} />
                        </div>
                        <div className="grid-lable-name">
                            <h2>{item.title}</h2>
                        </div>
                        <div className="grid-lable-text">
                            <h3>{item.text}</h3>
                        </div>
                    </div>
                </div>
                :
                <div className="grid-item">
                    <div className="grid-mask">
                        <button className={wanted ? 'grid-plus wanted' : 'grid-plus'} onClick={this.addToWanted}><img src="./img/grid-plus.png" alt="plus" /></button>
                        <button className={favorites ? 'grid-like favorites' : 'grid-like'} onClick={this.addToFavorites}><img src="./img/grid-like.png" alt="like" /></button>
                    </div>
                    <div className="grid-item-img">
                        <img src={item.image} alt={item.title} />
                    </div>
                    <div className="grid-item-text">
                        <h3>{item.title}</h3>
                    </div>
                    <div className="grid-item-price">
                        <span>$ {item.price}</span>
                    </div>
                </div>
        );
    }
}
