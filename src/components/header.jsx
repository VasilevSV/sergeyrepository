import React from 'react';
import { Heart } from './heart';
import { Bag } from './bag';

export class Header extends React.Component {

    render() {
        const {bagCounter} = this.props;
        const {heartCounter} = this.props;

        return (
            <header>
                <div className="container">
                    <div className="header1">
                        <div className="logo">
                            <a href="#Logo"><img src="./img/logo.png" alt="logo" /></a>
                        </div>
                        <div className="navigation">
                            <ul>
                                <li><a href="#Categories">Categories</a></li>
                                <li><a href="#About">About</a></li>
                                <li><a href="#Contact">Contact</a></li>
                            </ul>
                        </div>
                        <div className="settings">
                            <select>
                                <option>EN</option>
                                <option>RU</option>
                            </select>
                            <select>
                                <option>$US</option>
                                <option>RUB</option>
                            </select>
                        </div>
                        <div className="icons">
                            <a href=""><img src="./img/search-icon.png" alt="search" /></a>
                            <a href=""><img src="./img/profile-icon.png" alt="profile" /></a>
                            <Heart counter={heartCounter} />
                            <Bag counter={bagCounter} />
                        </div>
                    </div>
                </div>
            </header>
        );

    };

};

