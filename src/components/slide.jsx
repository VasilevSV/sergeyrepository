import React from "react";

export const Slide = ({item, show}) => {
    return (
        <li className={show ? "slide showing" : "slide"} style={{backgroundImage: `url(${item.image})`}}>
            <div className="slide-description" >
                <h2>{item.title}</h2>
                <p>{item.description}</p>
                <button>View More</button>
            </div>
        </li>
    )
}