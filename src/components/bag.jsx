import React from 'react';

export const Bag = ({counter}) => (
    <a href="">
        <span>{counter}</span>
        <img src="./img/bag-icon.png" alt="bag" />
    </a>
);