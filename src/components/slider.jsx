import React from "react";
import {slides} from "./../sliders";
import {Slide} from "./slide";
import {SliderButton} from "./slider-button";


export class Slider extends React.Component {
    state = {
        activeSlide: 0
    };

    showPrevSlide = () => this.setActiveSlide((this.state.activeSlide || 0) - 1);
    showNextSlide = () => this.setActiveSlide((this.state.activeSlide || 0) + 1);
    showPartSlide = (index) => {
        console.log(index);
        this.setActiveSlide(index);
    }

    setActiveSlide = (show) => {
        this.setState({activeSlide: (show+slides.length)%slides.length});
    }

    render() {
        const {activeSlide} = this.state;

        return (
            <div className="slider" >
                
                <div className="slider-buttons">
                    {slides.map((i, index) => <SliderButton
                        onClick={this.showPartSlide}
                        index={index}
                        style={activeSlide === index ? { background: '#7961a6' } : {}}
                        key={i.title} />)}
                </div>
                <ul className="slides">
                    {slides.map((item, index) => <Slide
                        item={item}
                        show={activeSlide === index}
                        key={item.title} />)}
                </ul>
                <button className="controls-left" onClick={this.showPrevSlide}><img src="./img/button-left.png" alt="button-left"/></button>
                <button className="controls-right" onClick={this.showNextSlide}><img src="./img/button-right.png" alt="button-right"/></button>
            </div>
                 );
            };
};
