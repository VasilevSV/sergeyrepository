export const Products = [
    {
        image: "./img/grid-1.jpg",
        title: "Kristina Dam Oak Table With White Marble Top",
        price: 799.55
    },
    {
        image: "./img/grid-2.jpg",
        title: "Hay - About A Lounge  Chair AAL 93",
        price:  659.55
    },
    {
        image: "./img/grid-3.jpg",
        title: "Activate Facial Mask and Charcoal Soap T",
        price:  129.55
    },
    {
        image: "./img/grid-4.jpg",
        title: "Cocktail Table Walnut  | YES",
        price:  299.99
    },
    {
        image: "./img/grid-5.jpg",
        title: "Hay - About A Lounge  Chair AAL 94",
        price:  659.55
    },
    {
        image: "./img/grid-6.jpg",
        title: "TORY DESK CALENDAR C",
        price:  410.99
    },
    {
        image: "./img/grid-7.jpg",
        title: "CH445 Wing Chair on  SUITE NY",
        price:  330.55
    },
    {
        lable: "kitchen",
        title: "for kitchen",
        image: "./img/for-kitchen-icon.png",
        text: "At vero eos et  accusamus et iusto odio"
    },
    {
        image: "./img/grid-9.jpg",
        title: "Kristina Dam Oak Table With White Marble Top SC",
        price:  2195.00
    },
    {
        image: "./img/grid-10.jpg",
        title: "MONOQI | Ø55 Crossit  Table - Purpl/Grn",
        price:  299.99
    },
    {
        image: "./img/grid-11.jpg",
        title: "Vitra Cork Stool C, Cork - Design Within Reach",
        price:  870.95
    },
    {
        image: "./img/grid-12.jpg",
        title: "Activate Facial Mask and Charcoal Soap",
        price:  129.55
    },
    {
        lable: "accesories",
        title: "Designer Accesories",
        image: "./img/designer-accesories-icon.png",
        text: "At vero eos et  accusamus et iusto odio"
    },
    {
        image: "./img/grid-14.jpg",
        title: "TORY DESK CALENDAR",
        price:  410.99
    },
    {
        image: "./img/grid-15.jpg",
        title: "EAMES x Cast + Crew - Custom Powder Coated",
        price:  330.55
    },
    {
        image: "./img/grid-16.jpg",
        title: "CH445 Wing Chair on  SUITE LA",
        price:  330.55
    },
    {
        image: "./img/grid-17.jpg",
        title: "Arkys Chair - Orange",
        price:  2195.00
    },
    {
        image: "./img/grid-18.jpg",
        title: "MONOQI | Ø55 Crossit  Table - Wht/Red",
        price:  299.99
    },
    {
        image: "./img/grid-19.jpg",
        title: "Vitra Cork Stool B, Cork - Design Within Reach",
        price:  870.95
    },
    {
        image: "./img/grid-20.jpg",
        title: "Activate Facial Mask and Charcoal Soap NY",
        price:  129.55
    },

]